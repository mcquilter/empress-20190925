#!/bin/bash
set -eux
if [[ -e ./images ]]; then
  echo 'images already exists'
  touch ./images/exists
  rm ./images/exists
else
  mkdir -p ./images
fi
jpegoptim -m 60 -p -d ./images ./orig/*.JPG
if [[ -e ./orig/darktable_exported ]]; then
  jpegoptim -m 60 -p -d ./images ./orig/darktable_exported/*.jpg
fi
